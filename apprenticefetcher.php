<?php  // recup les data (Model)
 class ApprenticeFetcher {
    public $apiC26;

    public function __construct($apiC26) 
    {
        $this->apiC26 = $apiC26;
    }

    public function getApprenticeData() {
        $data = $this->fetchData();
        return $this->decode($data);
    }

    private function fetchData() {
        $curl = curl_init($this->apiC26);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $data = curl_exec($curl);
        if(!$data) {
            throw new Error("Oh non pas l'api");
        }

        return $data;
    }

    private function decode($jsonString) {
        return json_decode($jsonString);
    }

}